db.fruits.aggregate([

    {$match:{$and:[{price:{$lt:50}},{supplier:"Yellow Farms"}]}},
    {$count: "YellowFarmsPriceLessThan50"}

])

db.fruits.aggregate([

    {$match:  {price:{$lt:30}}},
    {$count: "totalItems"}

])

db.fruits.aggregate([

    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"Yellow Farms",avgPrice:{$avg:"$price"}} }

])

db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.",maxPrice:{$max:"$price"}} }

])

db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.",minPrice:{$min:"$price"}} }

])
